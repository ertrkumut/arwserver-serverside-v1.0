package main

var arwServer *ARWServer

func main() {
	arwServer = new(ARWServer)

	arwServer.AddEventHandler(&(arwServer.events.Login), Login_Event_Handler)
	arwServer.Initialize()

	arwServer.ProcessEvents()
}

func Login_Event_Handler(arwObj ARWObject) {

	user, _ := arwObj.GetUser(arwServer)

	newRoom := arwServer.roomManager.SearchRoomWithTag("Game")
	if newRoom != nil {
		newRoom.AddUserInRoom(user, arwServer)
	} else {
		var roomSettings RoomSettings
		roomSettings.tag = "Game"

		room := arwServer.roomManager.CreateRoom(arwServer, &roomSettings)
		room.AddUserInRoom(user, arwServer)
	}
}
